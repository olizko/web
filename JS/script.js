const windowOne = document.querySelector('.window#one')
const windowTwo = document.querySelector('.window#two')
const windowThree = document.querySelector('.window#three')
const windowFour = document.querySelector('.window#four')

const buttonOneNext = windowOne.querySelector('button.button')
buttonOneNext.onclick = () => {
  windowOne.classList.remove('current')
  windowTwo.classList.add('current')
}

const buttonTwoNext = windowTwo.querySelector('#nextTwo')
buttonTwoNext.onclick = () => {
  windowTwo.classList.remove('current')
  windowThree.classList.add('current')
}

const buttonTwoBack = windowTwo.querySelector('#backTwo')
buttonTwoBack.onclick = () => {
  windowTwo.classList.remove('current')
  windowOne.classList.add('current')
}

const buttonThreeNext = windowThree.querySelector('#nextThree')
buttonThreeNext.onclick = () => {
  windowThree.classList.remove('current')
  windowFour.classList.add('current')
}

const buttonThreeBack = windowThree.querySelector('#backThree')
buttonThreeBack.onclick = () => {
  windowThree.classList.remove('current')
  windowFour.classList.add('current')
}
